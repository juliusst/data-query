package lt.js.dataquery.bean;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lt.js.dataquery.entity.DataQuery;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class QueryRequest {

	private @Getter String id, title, content;
	private @Getter Integer views, timestamp;

	public DataQuery convertToDataQuery() {
		return new DataQuery(id, title, content, views, timestamp);
	}

}

package lt.js.dataquery.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lt.js.dataquery.entity.DataQuery;

@Repository
public interface DataQueryRepositoryBasic extends JpaRepository<DataQuery, Long> {

	@Query("from DataQuery where id=?1")
	<T> Optional<T> findById(String id);

	@Modifying
	@Transactional
	@Query("update DataQuery set title=?1, content =?2, views=?3, timestamp=?4  where id =?5")
	void update(String title, String content, Integer views, Integer timestamp, String id);
}

package lt.js.dataquery.dao;

import org.springframework.stereotype.Repository;

@Repository
public interface DataQueryRepository extends DataQueryRepositoryBasic, DataQueryRepositoryCustom {

}

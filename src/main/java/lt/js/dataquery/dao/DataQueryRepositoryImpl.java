package lt.js.dataquery.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.js.dataquery.entity.DataQuery;
import lt.js.dataquery.error.BadSqlStatementException;

public class DataQueryRepositoryImpl implements DataQueryRepositoryCustom {

	private static final Logger logger = LoggerFactory.getLogger(DataQueryRepositoryImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<DataQuery> findByWhereClause(String whereClause) {
		String hql = String.format("%s %s", "from DataQuery where", whereClause);
		try {
			List<DataQuery> result = entityManager.createQuery(hql, DataQuery.class).getResultList();
			logger.debug("Data query found='{}'", result.size());
			return result;
		} catch (Exception e) {
			logger.error("error ='{}'", e);
			throw new BadSqlStatementException(e.getMessage());
		}
	}

}

package lt.js.dataquery.util;

public final class Messages {

	private Messages() {
	}

	public static final String EMPTY_PARAM = "Param %s can't be empty!";
	public static final String MAX_PARAM_LENGTH = "Param %s max length %d!";
	public static final String BAD_SQL_QUERY = "Error executing sql query! error stack: %s";
	public static final String OBJECT_TO_JSON_ERROR = "Can't convert from object to Json! Error message: %s";
}

package lt.js.dataquery.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class DataToSqlQueryParser implements ToSqlQueryParser {

	private static Map<String, String> VALUES_TO_REPLACE = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put("query=", "");
			put(")EQUAL", ")|EQUAL");
			put("AND", "|AND");
			put("OR", "|OR");
			put("NOT", "|NOT");
			put("GREATER_THAN", "|GREATER_THAN");
			put("LESS_THAN", "|LESS_THAN");
		}
	};

	@Override
	public String convertQueryToSql(String query) {
		List<String> splittedByOperatorQueries = splitQueryToSeparateQueries(query);

		String sqlQuery = "1=1";
		for (String dataQuery : splittedByOperatorQueries) {
			if (dataQuery.startsWith("EQUAL")) {
				sqlQuery += convertEqual(dataQuery);
			} else if (dataQuery.startsWith("AND")) {
				sqlQuery += convertAnd(dataQuery);
			} else if (dataQuery.startsWith("OR")) {
				sqlQuery += convertOr(dataQuery);
			} else if (dataQuery.startsWith("NOT")) {
				sqlQuery += convertNot(dataQuery);
			} else if (dataQuery.startsWith("GREATER_THAN")) {
				sqlQuery += convertGreaterThan(dataQuery);
			} else if (dataQuery.startsWith("LESS_THAN")) {
				sqlQuery += convertLessThan(dataQuery);
			}
		}
		return convertToSqlSyntax(sqlQuery);
	}

	private String convertToSqlSyntax(String query) {
		return query.replace("\"", "'").replace(",", " = ");
	}

	private String convertEqual(String query) {
		return query.replace("EQUAL", " and ");
	}

	private String convertAnd(String query) {
		return query.replace("AND", " and ").replace("EQUAL", "").replace("),(", ") and (");
	}

	private String convertOr(String query) {
		return query.replace("OR", " and ").replace("EQUAL", "").replace("),(", ") or (");
	}

	private String convertNot(String query) {
		return query.replace("NOT", " and ").replace("EQUAL", "").replace(",", "<>");
	}

	private String convertGreaterThan(String query) {
		return query.replace("GREATER_THAN", " and ").replace(",", ">");
	}

	private String convertLessThan(String query) {
		return query.replace("LESS_THAN", " and ").replace(",", "<");
	}

	private List<String> splitQueryToSeparateQueries(String url) {
		String urlWithDelimiter = url;
		for (String name : VALUES_TO_REPLACE.keySet()) {
			urlWithDelimiter = urlWithDelimiter.replace(name, VALUES_TO_REPLACE.get(name));
		}
		return Arrays.asList(urlWithDelimiter.split(Pattern.quote("|")));
	}

}

package lt.js.dataquery.util;

public interface ToSqlQueryParser {

	String convertQueryToSql(String query);

}
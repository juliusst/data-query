package lt.js.dataquery.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "data_query")
@NoArgsConstructor
public class DataQuery {

	@SequenceGenerator(name = "StoreSeq", sequenceName = "storeSequence", initialValue = 1, allocationSize = 100)
	@GeneratedValue(generator = "StoreSeq")
	@Id
	@Column(nullable = false)
	private long no;

	@Column(unique = true, nullable = false)
	private @Getter String id;

	@Column(length = 2000)
	private @Getter String title, content;

	private @Getter Integer views, timestamp;

	public DataQuery(String id, String title, String content, Integer views, Integer timestamp) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.views = views;
		this.timestamp = timestamp;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		DataQuery dataQuery = (DataQuery) obj;
		return id.equals(dataQuery.id) && title.equals(dataQuery.title) && content.equals(dataQuery.content)
				&& views.equals(dataQuery.views) && timestamp.equals(dataQuery.timestamp);
	}
}

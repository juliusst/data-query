package lt.js.dataquery.error;

import static lt.js.dataquery.util.Messages.BAD_SQL_QUERY;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(BAD_REQUEST)
public class BadSqlStatementException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BadSqlStatementException(String error) {
		super(String.format("%s %s", BAD_SQL_QUERY, error));
	}

}

package lt.js.dataquery.controller;

import lt.js.dataquery.bean.QueryRequest;
import lt.js.dataquery.entity.DataQuery;
import lt.js.dataquery.service.DataQueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/store")
@ResponseStatus(HttpStatus.OK)
public class DataQueryController {

    private static final Logger logger = LoggerFactory.getLogger(DataQueryController.class);

    private final DataQueryService service;

    public DataQueryController(DataQueryService service) {
        this.service = service;
    }

    @GetMapping
    public List<DataQuery> getDataQuery(@RequestParam String query) {
        logger.debug("Method getDataQuery query ='{}'", query);
        return service.getDataQuery(query);
    }

    @PostMapping
    public void postDataQuery(@RequestBody QueryRequest request) {
        logRequest(request);
        service.postDataQuery(request);
    }

    private void logRequest(QueryRequest request) {
        logger.debug("Method postDataQuery id ='{}' title ='{}' length content ='{}'", request.getId(),
                request.getTitle(), request.getContent().length());
    }

}
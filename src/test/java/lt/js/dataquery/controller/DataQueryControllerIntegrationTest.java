package lt.js.dataquery.controller;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import lt.js.dataquery.DataQueryApplication;
import lt.js.dataquery.bean.QueryRequest;
import lt.js.dataquery.dao.DataQueryRepository;
import lt.js.dataquery.entity.DataQuery;
import lt.js.dataquery.error.OjectConvertToJsonException;

@AutoConfigureMockMvc
@SpringBootTest(classes = DataQueryApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DataQueryControllerIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private DataQueryRepository dataQueryRepository;

	@Test
	public void postDataQuery_ReturnsHttpStatusOK_IfRequestBodyIsValid() throws Exception {
		callPostServiceAndReturnHttpStatusOK(MockMvcResultMatchers.status().isOk(), createRequest());
	}

	@Test
	public void getDataQuery_ReturnsDataQuery_IfQueryParamsExistsInDb() throws Exception {
		DataQuery dataQuery = createRequest().convertToDataQuery();
		dataQueryRepository.save(dataQuery);
		String query = "query=Equal(id,\"first-post\")";
		List<DataQuery> response = callGetService(MockMvcResultMatchers.status().isOk(), query);
		assertTrue(dataQuery.equals(response.get(0)));
	}

	private void callPostServiceAndReturnHttpStatusOK(ResultMatcher expectedResult, QueryRequest request)
			throws Exception {
		String json = convertObjectToJson(request);
		mvc.perform(MockMvcRequestBuilders.post("/store").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(expectedResult);
	}

	private List<DataQuery> callGetService(ResultMatcher expectedResult, String query) throws Exception {
		ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get(String.format("%s%s", "/store?", query))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(expectedResult);
		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		return objectMapper.readValue(contentAsString, new TypeReference<>() {
		});
	}

	private QueryRequest createRequest() {
		return new QueryRequest("first-post", "My   First   Post", "Hello   World!", 1, 1555832341);
	}

	private String convertObjectToJson(QueryRequest request) {
		ObjectWriter writer = objectMapper.writer().withDefaultPrettyPrinter();
		try {
			return writer.writeValueAsString(request);
		} catch (JsonProcessingException e) {
			throw new OjectConvertToJsonException(e.getMessage());
		}
	}

}

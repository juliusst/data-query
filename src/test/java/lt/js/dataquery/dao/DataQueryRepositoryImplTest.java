package lt.js.dataquery.dao;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import lt.js.dataquery.entity.DataQuery;
import lt.js.dataquery.error.BadSqlStatementException;

@DataJpaTest
public class DataQueryRepositoryImplTest {

	@Autowired
	private DataQueryRepository dataQueryRepository;

	@Test
	public void findByWhereClause_ThrowsBadRequestException_IfWhereClauseIsNotValid() {
		Assertions.assertThrows(BadSqlStatementException.class, () -> {
			dataQueryRepository.findByWhereClause("Test");
		});
	}

	@Test
	public void findByWhereClause_ReturnEmptyArray_IfWhereClauseIsValid() {
		List<DataQuery> result = dataQueryRepository
				.findByWhereClause("id = '1' and title = 'Testss' and content = 101 or views = 101");
		assertTrue(result.isEmpty());
	}
}

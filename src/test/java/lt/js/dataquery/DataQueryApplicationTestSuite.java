package lt.js.dataquery;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages({ "lt.js.dataquery.controller", "lt.js.dataquery.dao", "lt.js.dataquery.util" })
public class DataQueryApplicationTestSuite {

}

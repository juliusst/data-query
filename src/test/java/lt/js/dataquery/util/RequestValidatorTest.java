package lt.js.dataquery.util;

import java.util.Base64;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import lt.js.dataquery.bean.QueryRequest;
import lt.js.dataquery.error.EmptyValueException;
import lt.js.dataquery.error.MaxValueLengthException;

public class RequestValidatorTest {

    private final RequestValidator validator = new RequestValidator();

    private final String requestParam = formatLongRequestParam();

    @Test
    public void validate_ThrowsEmptyValueException_IfParameterIdIsEmpty() {
        QueryRequest queryRequest = new QueryRequest(null, "My   First   Post", requestParam, 1, 1555832341);
        Assertions.assertThrows(EmptyValueException.class, () -> validator.validate(queryRequest));
    }

    @Test
    public void validate_ThrowsMaxValueLengthException_IfParameterIdLengthTooLong() {
        QueryRequest queryRequest = new QueryRequest(requestParam, "My   First   Post", "Hello   World!", 1, 1555832341);
        Assertions.assertThrows(MaxValueLengthException.class, () -> validator.validate(queryRequest));
    }

    @Test
    public void validate_ThrowsMaxValueLengthException_IfParameterTitleLengthTooLong() {
        QueryRequest queryRequest = new QueryRequest("first-post", requestParam, "Hello   World!", 1, 1555832341);
        Assertions.assertThrows(MaxValueLengthException.class, () -> {
            validator.validate(queryRequest);
        });
    }

    @Test
    public void validate_ThrowsMaxValueLengthException_IfParameterContentLengthTooLong() {
        QueryRequest queryRequest = new QueryRequest("1", "1010", requestParam, 1, 1555832341);
        Assertions.assertThrows(MaxValueLengthException.class, () -> {
            validator.validate(queryRequest);
        });
    }

    @Test
    public void validate_ThrowsEmptyValueException_IfParameterQueryIsEmpty() {
        Assertions.assertThrows(EmptyValueException.class, () -> {
            validator.validate("   ");
        });
    }

    private String formatLongRequestParam() {
        byte[] bytes = new byte[2100];
        return Base64.getEncoder().encodeToString(bytes);
    }

}
